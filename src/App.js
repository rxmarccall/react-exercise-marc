import React from 'react'

import Representatives from './components/Representatives';

import Container from '@mui/material/Container';
import Card from '@mui/material/Card';

// Create App component
function App()
{
  return (
    <div classlabel="app">
      <Container sx={{ minWidth: 1000, mt: 8, overflow: 'visible' }}>
        <Card sx = {{ minHeight:1000, backgroundColor: '#6C6ADB'}}>
          <Representatives />
        </Card>
      </Container>
    </div>
  )
}

export default App