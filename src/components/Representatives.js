import React, { useState } from 'react'
import Select from 'react-select'

import Card from '@mui/material/Card';
import Box from '@mui/material/Box';
import FormLabel from '@mui/material/FormLabel';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';

import {states} from './../data/states'

const Representatives = (props) => {

    const [rep, setReps] = useState([]);
    const [searchType, setSearchType] = useState();
    
    const searchByOptions = [{value: 'Representatives', label: 'Representatives'}, {value: 'Senators', label: 'Senators'}];

    return (
        <div>
            {/* Header */}
            <Card sx={{ m: 4, backgroundColor: '#403E9E' }}>
                <h2 align="center" style={{ color: "white" }}>USA Representatives Search</h2>
            </Card>

            {/* Search by Representative or Senator? */}
            <Box sx={{ m: 4 }}>
                <Box sx={{ mt: 2 }}>
                    <Box sx={{ mb: 1 }}>
                        <FormLabel style={{ color: "white" }}>Search By</FormLabel>
                    </Box>
                    <Select
                            id="SearchType"
                            options={searchByOptions}
                            onChange={(val) => setSearchType(val)}
                        >
                    </Select>
                </Box>
                <Box sx={{ mt: 2 }}>
                    <Box sx={{ mb: 1 }}>
                        <FormLabel style={{ color: "white" }}>State</FormLabel>
                    </Box>
                    <Select
                        id="State"
                        isDisabled={searchType === undefined}
                        options={states}
                        onChange={(val) => { searchType.value === 'Representatives' ? getRepresentativesByState(val, setReps) : getSenatorsByState(val, setReps)}}
                    >
                    </Select>
                </Box>
            </Box>

            {/* Table to display representative data */}
            <Box sx={{ flexDirection: 'column', m: 4}}>
                <h2 sx={{mt: 2}} style={{color: "white"}}>Who Represents You?</h2>
                <Card>
                    <TableContainer>
                        <Table sx={{ minWidth: 650 }} aria-label="simple table">
                            <TableHead sx={{ background: "#E3E2FB" }}>
                                <TableRow>
                                    <TableCell sx={{ fontWeight: 'bold'}}>Party</TableCell>
                                    <TableCell sx={{ fontWeight: 'bold'}}>First Name</TableCell>
                                    <TableCell sx={{ fontWeight: 'bold'}}>Last Name</TableCell>
                                    <TableCell sx={{ fontWeight: 'bold'}}>District</TableCell>
                                    <TableCell sx={{ fontWeight: 'bold'}}>Phone</TableCell>
                                    <TableCell sx={{ fontWeight: 'bold'}}>Office</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {rep?.map((x, index) => (
                                    <TableRow
                                        key={x.name} hover
                                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                        style ={{cursor:"pointer", background: getRowColor(index)}}
                                        onClick={() => openRepresentativeURL(x.link)}
                                        >
                                        <TableCell component="th" scope="row">
                                            {x.party}
                                        </TableCell>
                                        <TableCell component="th" scope="row">
                                            {x.name.substring(0, x.name.indexOf(' '))}
                                        </TableCell>
                                        <TableCell component="th" scope="row">
                                            {x.name.substring(x.name.indexOf(' '))}
                                        </TableCell>
                                        <TableCell component="th" scope="row">
                                            {x.district}
                                        </TableCell>
                                        <TableCell component="th" scope="row">
                                            {x.phone}
                                        </TableCell>
                                        <TableCell component="th" scope="row">
                                            {x.office}
                                        </TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Card>
            </Box>
        </div>
    )
}

// Helper Methods
const openRepresentativeURL = (url) => {
    window.open(url);
}

const getRowColor = (index) => {
    return (index % 2? "#F2F2FF" : "white");
}

const getRepresentativesByState = async (param, setReps) => {
    await fetch('/representatives/' + param.value)
      .then((res) => res.json().then(data => {
        setReps(data.results);
      }));
}

const getSenatorsByState = async (param, setReps) => {
    await fetch('/senators/' + param.value)
      .then((res) => res.json().then(data => {
        setReps(data.results);
      }));
}

export default Representatives;